role-wait-for-host
=========

This is a role to wait for an linux instance or virtual machine to become available

Requirements
------------

This role requires an instance_name

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - name: Wait for linux instance to become available
    - hosts: servers
      instance_name: example
      include_role:
         - name: role-wait-for-host
Variables
---------

| Variable Name | Description | Mandatory |
|--|--|--|
| instance_name | Name of the VM or Instance | yes |

TODO
----

Support more operating systems. namely windows.

